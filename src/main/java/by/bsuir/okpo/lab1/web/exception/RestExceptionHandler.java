package by.bsuir.okpo.lab1.web.exception;

import java.util.ResourceBundle;

import by.bsuir.okpo.lab1.service.exception.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

    private static final ResourceBundle MESSAGES = ResourceBundle.getBundle("error-messages");
    private static final String MESSAGE_POSTFIX = ".MESSAGE";
    private static final String INTERNAL_SERVER_ERROR = "OKPO-1000";

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceException.class)
    public ErrorResponse serviceException(ServiceException exception) {
        String errorCode = exception.getErrorCode().getCode();
        String errorMessage = MESSAGES.getString(errorCode + MESSAGE_POSTFIX);

        return new ErrorResponse(errorCode, errorMessage);
    }   

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse exception(Exception exception) {
        String errorMessage = MESSAGES.getString(INTERNAL_SERVER_ERROR + MESSAGE_POSTFIX);

        return new ErrorResponse(INTERNAL_SERVER_ERROR, errorMessage);
    }
}
