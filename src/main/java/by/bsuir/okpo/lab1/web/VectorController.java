package by.bsuir.okpo.lab1.web;

import java.util.List;

import by.bsuir.okpo.lab1.model.Vector;
import by.bsuir.okpo.lab1.service.VectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/vectors", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class VectorController {

    private final VectorService vectorService;

    @Autowired
    public VectorController(VectorService vectorService) {
        this.vectorService = vectorService;
    }

    @PostMapping("/composition")
    public Vector compose(@RequestBody List<Vector> vectors) {
//        System.out.println(Thread.currentThread().getName());
        return vectorService.composeVectors(vectors);
    }
}
