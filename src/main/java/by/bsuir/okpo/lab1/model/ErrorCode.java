package by.bsuir.okpo.lab1.model;

public interface ErrorCode {

    String getCode();
}
