package by.bsuir.okpo.lab1.model;

import java.util.List;

public class Vector {

    private List<Double> values;

    public Vector() {
    }

    public Vector(List<Double> values) {
        this.values = values;
    }

    public List<Double> getValues() {
        return values;
    }

    public void setValues(List<Double> values) {
        this.values = values;
    }
}
