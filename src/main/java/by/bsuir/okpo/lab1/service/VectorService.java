package by.bsuir.okpo.lab1.service;

import java.util.ArrayList;
import java.util.List;

import by.bsuir.okpo.lab1.model.Vector;
import by.bsuir.okpo.lab1.service.validator.VectorValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VectorService {

    private final VectorValidator vectorValidator;

    @Autowired
    public VectorService(VectorValidator vectorValidator) {
        this.vectorValidator = vectorValidator;
    }

    public Vector composeVectors(List<Vector> vectors) {
        vectorValidator.validate(vectors);

        int vectorSize = vectors.get(0).getValues().size();
        List<Double> result = new ArrayList<>(vectorSize);
        for (int i = 0; i < vectorSize; i++) {
            result.add(0D);
        }

        vectors.forEach(vector -> {
            List<Double> values = vector.getValues();
            for (int i = 0; i < values.size(); i++) {
                double sum = result.get(i) + values.get(i);
                result.set(i, sum);
            }
        });

        return new Vector(result);
    }
}
