package by.bsuir.okpo.lab1.service.exception;

import by.bsuir.okpo.lab1.model.ErrorCode;

public enum ServiceErrorCodes implements ErrorCode {

    VECTORS_HAVE_DIFFERENT_DIMENSIONS(1001),
    VECTORS_ARE_EMPTY(1002);

    private static final String DOMAIN_PREFIX = "OKPO-";

    private final String errorCode;

    ServiceErrorCodes(Integer code) {
        errorCode = DOMAIN_PREFIX + code;
    }

    @Override
    public String getCode() {
        return errorCode;
    }
}
