package by.bsuir.okpo.lab1.service.validator;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import by.bsuir.okpo.lab1.model.Vector;
import by.bsuir.okpo.lab1.service.exception.ServiceErrorCodes;
import by.bsuir.okpo.lab1.service.exception.ServiceException;
import org.springframework.stereotype.Component;

@Component
public class VectorValidator {

    public void validate(List<Vector> vectors) {
        long sizes = Optional.ofNullable(vectors)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(Vector::getValues)
                .mapToInt(Collection::size)
                .distinct()
                .count();
        if (sizes == 0) {
            throw new ServiceException(ServiceErrorCodes.VECTORS_ARE_EMPTY);
        } else if (sizes > 1) {
            throw new ServiceException(ServiceErrorCodes.VECTORS_HAVE_DIFFERENT_DIMENSIONS);
        }
    }
}
