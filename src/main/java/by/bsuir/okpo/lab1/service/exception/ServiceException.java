package by.bsuir.okpo.lab1.service.exception;

public class ServiceException extends RuntimeException {

    private final ServiceErrorCodes errorCode;

    public ServiceException(ServiceErrorCodes errorCode) {
        this.errorCode = errorCode;
    }

    public ServiceErrorCodes getErrorCode() {
        return errorCode;
    }
}
